"use client";

import { ThemeProvider, createTheme } from "@mui/material/styles";
import { ReactNode } from "react";

let theme = createTheme({
  palette: {
    primary: {
      main: "#F3E2E2",
    },
    secondary: {
      main: "#D9D9D9",
    },
    background: {
      default: "#FCF8F8",
    },
  },
});

theme = createTheme(theme, {
  palette: {
    info: {
      main: "#ffffff",
    },
  },
});

export default function ThemeWrapper({ children }: { children: ReactNode }) {
  return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
}
