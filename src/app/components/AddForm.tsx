import React, { useEffect, useState } from "react";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  TextField,
  DialogActions,
  Button,
  Box,
} from "@mui/material";
import * as Yup from "yup";

interface AddFormProps {
  open: boolean;
  onClose: () => void;
  onAddBook: (book: {
    title: string;
    author: { name: string };
    totalCopies: number;
  }) => void;
}

const AddForm: React.FC<AddFormProps> = ({ open, onClose, onAddBook }) => {
  const [title, setTitle] = useState("");
  const [authorName, setAuthorName] = useState("");
  const [totalCopies, setTotalCopies] = useState(1);
  const [error, setError] = useState<{ [key: string]: string }>({});

  const validationSchema = Yup.object().shape({
    title: Yup.string().required("אנא הכנס את שם הספר"),
    authorName: Yup.string()
      .required("אנא הכנס את שם הסופר")
      .matches(/^[a-zA-Z\-\u0590-\u05FF ]*$/, "שם הסופר יכול להכיל רק אותיות"),
    totalCopies: Yup.number()
      .required("אנא הכנס את מספר העותקים")
      .positive("אנא הכנס מספר חוקי")
      .integer("אנא הכנס מספר חוקי"),
  });

  useEffect(() => {
    if (!open) {
      setTitle("");
      setAuthorName("");
      setTotalCopies(1);
      setError({});
    }
  }, [open]);

  const handleSubmit = async () => {
    try {
      await validationSchema.validate(
        { title, authorName, totalCopies },
        { abortEarly: false }
      );
      onAddBook({
        title,
        author: { name: authorName },
        totalCopies,
      });
      onClose();
    } catch (validationErrors) {
      const formattedErrors: { [key: string]: string } = {};
      if (validationErrors instanceof Yup.ValidationError) {
        validationErrors.inner.forEach((error) => {
          if (error.path) {
            formattedErrors[error.path] = error.message;
          }
        });
      }
      setError(formattedErrors);
    }
  };

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle fontFamily={"Segoe UI"} sx={{ backgroundColor: "#F9F1F1" }}>
        הוספת ספר חדש
      </DialogTitle>
      <DialogContent sx={{ backgroundColor: "#F9F1F1" }}>
        <Box component="form" noValidate autoComplete="off">
          <TextField
            margin="dense"
            label="כותרת"
            type="text"
            fullWidth
            value={title}
            onChange={(e) => setTitle(e.target.value)}
            error={!!error.title}
            helperText={error.title}
          />
          <TextField
            margin="dense"
            label="שם הסופר"
            type="text"
            fullWidth
            value={authorName}
            onChange={(e) => setAuthorName(e.target.value)}
            error={!!error.authorName}
            helperText={error.authorName}
          />
          <TextField
            margin="dense"
            label="מספר עותקים"
            type="number"
            fullWidth
            value={totalCopies}
            onChange={(e) => setTotalCopies(Number(e.target.value))}
            error={!!error.totalCopies}
            helperText={error.totalCopies}
          />
        </Box>
      </DialogContent>
      <DialogActions sx={{ backgroundColor: "#F9F1F1" }}>
        <Button
          onClick={onClose}
          className="cancelBtn"
          sx={{ backgroundColor: "#D9D9D9", color: "black" }}
        >
          ביטול
        </Button>
        <Button
          onClick={handleSubmit}
          className="okBtn"
          sx={{ backgroundColor: "#B96D6D" }}
        >
          אישור
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default AddForm;
