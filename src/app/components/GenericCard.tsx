import React, { useState } from "react";
import {
  Box,
  Button,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardMedia,
  Typography,
  Modal,
  useTheme,
  Grid,
  List,
  ListItem,
  ListItemText,
  Divider,
  Select,
  MenuItem,
  FormControl,
  InputLabel,
} from "@mui/material";
import InfoIcon from "@mui/icons-material/InfoOutlined";
import {
  useGetBorrowingsQuery,
  useGetUsersQuery,
  useBorrowBookMutation,
  useGetBooksQuery,
} from "../store/api";

const appBarHeight = 64;

interface CardProps {
  image: string;
  title: string;
  subtitle: string;
  additionalInfo: string;
  additionalInfo2: string;
  bookId: number;
}

const GenericCard: React.FC<CardProps> = ({
  image,
  title,
  subtitle,
  additionalInfo,
  additionalInfo2,
  bookId,
}) => {
  const theme = useTheme();
  const [drawerOpen, setDrawerOpen] = useState(false);
  const [borrowModalOpen, setBorrowModalOpen] = useState(false);
  const [selectedUser, setSelectedUser] = useState<number | null>(null);

  const {
    data: borrowings,
    isLoading: isLoadingBorrowings,
    isError: isErrorBorrowings,
    refetch: refetchBorrowings,
  } = useGetBorrowingsQuery(bookId);

  const {
    data: users,
    isLoading: isLoadingUsers,
    isError: isErrorUsers,
  } = useGetUsersQuery();

  const { refetch: refetchBooks } = useGetBooksQuery();

  const [borrowBook] = useBorrowBookMutation();

  const toggleDrawer = (open: boolean) => () => {
    setDrawerOpen(open);
  };

  const toggleBorrowModal = (open: boolean) => () => {
    setBorrowModalOpen(open);
  };

  const handleBorrowClick = async () => {
    if (selectedUser !== null) {
      try {
        await borrowBook({ userId: selectedUser, bookId }).unwrap();
        alert("השאלת הספר הושלמה");
        refetchBorrowings();
        refetchBooks();
        setBorrowModalOpen(false);
      } catch (error) {
        alert("השאלת הספר נכשלה");
      }
    } else {
      alert("בחר משתמש");
    }
  };

  return (
    <>
      <Card
        sx={{
          width: "90%",
          height: "100%",
          backgroundColor: theme.palette.primary.main,
          fontFamily: theme.typography.fontFamily,
          borderRadius: theme.shape.borderRadius,
        }}
      >
        <CardActionArea>
          <Box
            sx={{
              backgroundColor: theme.palette.secondary.main,
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <CardMedia
              sx={{ height: 150, width: "40%", objectFit: "contain" }}
              component="img"
              image={image}
              alt={title}
            />
          </Box>
          <CardContent>
            <Typography
              gutterBottom
              variant="h5"
              component="div"
              fontFamily={theme.typography.fontFamily}
              fontWeight="bold"
            >
              {title}
            </Typography>
            <Typography
              variant="body2"
              color="text.secondary"
              fontFamily={theme.typography.fontFamily}
              fontWeight="bold"
            >
              {subtitle}
            </Typography>
            <Typography
              variant="body2"
              color="text.secondary"
              fontFamily={theme.typography.fontFamily}
              fontWeight="bold"
            >
              {additionalInfo}
            </Typography>
          </CardContent>
        </CardActionArea>
        <CardActions>
          <Button
            className="info"
            size="small"
            color="primary"
            onClick={toggleDrawer(true)}
          >
            <InfoIcon sx={{ color: theme.palette.grey[600] }} />
          </Button>
        </CardActions>
      </Card>

      <Modal
        open={drawerOpen}
        onClose={toggleDrawer(false)}
        sx={{
          top: appBarHeight,
          direction: "ltr",
          textAlign: "right",
        }}
      >
        <Box
          sx={{
            padding: 3,
            width: "30%",
            height: "100%",
            bgcolor: "background.paper",
            borderRadius: theme.shape.borderRadius,
            overflowY: "auto",
          }}
        >
          <Grid container direction="row" spacing={2}>
            <Grid item xs={8}>
              <Typography
                variant="h6"
                fontFamily={theme.typography.fontFamily}
                fontWeight="bold"
              >
                {title}
              </Typography>
              <Typography
                variant="body2"
                fontWeight="bold"
                color="text.secondary"
                fontFamily={theme.typography.fontFamily}
                marginTop={1}
              >
                {subtitle}
              </Typography>
              <Typography
                variant="body2"
                color="text.secondary"
                fontFamily={theme.typography.fontFamily}
                marginTop={1}
              >
                {additionalInfo2}
              </Typography>
            </Grid>
            <Grid item xs={4}>
              <CardMedia
                sx={{
                  width: "100%",
                  objectFit: "contain",
                  backgroundColor: theme.palette.secondary.main,
                }}
                component="img"
                image={image}
                alt={title}
              />
            </Grid>
          </Grid>

          {isLoadingBorrowings && (
            <Grid item>
              <Typography>Loading borrowings...</Typography>
            </Grid>
          )}
          {isErrorBorrowings && (
            <Grid item>
              <Typography>שגיאה בטעינת קוראים</Typography>
            </Grid>
          )}
          {!isLoadingBorrowings && !isErrorBorrowings && borrowings && (
            <Grid item>
              <Box sx={{ marginTop: 2, textAlign: "right" }}>
                <Typography
                  variant="h6"
                  fontFamily={theme.typography.fontFamily}
                  fontWeight="bold"
                >
                  רשימת קוראים ({borrowings.length})
                </Typography>
                <List>
                  {borrowings.map((borrowing, index) => (
                    <React.Fragment key={borrowing.id}>
                      <ListItem
                        sx={{
                          direction: "rtl",
                        }}
                      >
                        <ListItemText
                          primary={`${borrowing.firstName} ${borrowing.lastName}`}
                          sx={{ textAlign: "right", flex: "1" }}
                        />
                        <Typography sx={{ marginLeft: "10px", flexShrink: 0 }}>
                          {borrowing.id}
                        </Typography>
                      </ListItem>
                      {index < borrowings.length - 1 && <Divider />}
                    </React.Fragment>
                  ))}
                </List>
              </Box>
            </Grid>
          )}

          <Button
            className="borrowBtn"
            onClick={toggleBorrowModal(true)}
            sx={{
              backgroundColor: "#B96D6D",
              marginTop: 2,
            }}
          >
            השאל
          </Button>
        </Box>
      </Modal>

      <Modal
        open={borrowModalOpen}
        onClose={toggleBorrowModal(false)}
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Box
          sx={{
            padding: 3,
            width: "30%",
            height: "30%",
            bgcolor: "#F9F1F1",
            borderRadius: theme.shape.borderRadius,
            overflowY: "auto",
          }}
        >
          <Typography
            variant="h6"
            fontFamily={theme.typography.fontFamily}
            fontWeight="bold"
          >
            השאל ספר
          </Typography>
          <FormControl fullWidth sx={{ marginTop: 2 }}>
            <InputLabel id="user-select-label">בחר משתמש</InputLabel>
            <Select
              labelId="user-select-label"
              value={selectedUser}
              onChange={(e) => setSelectedUser(e.target.value as number)}
              label="בחר משתמש"
            >
              {isLoadingUsers && <MenuItem value="">Loading users...</MenuItem>}
              {isErrorUsers && <MenuItem value="">Error</MenuItem>}
              {!isLoadingUsers &&
                !isErrorUsers &&
                users &&
                users.map((user) => (
                  <MenuItem key={user.id} value={user.id}>
                    {user.firstName} {user.lastName}
                  </MenuItem>
                ))}
            </Select>
          </FormControl>

          <Button
            className="borrowBtn"
            onClick={handleBorrowClick}
            sx={{
              backgroundColor: "#B96D6D",
              marginTop: 2,
            }}
          >
            השאל
          </Button>
          <Button
            className="cancelBtn"
            onClick={() => setBorrowModalOpen(false)}
            sx={{
              backgroundColor: "#D9D9D9",
              color: "black",
              marginRight: "10px",
              marginTop: 2,
            }}
          >
            ביטול
          </Button>
        </Box>
      </Modal>
    </>
  );
};

export default GenericCard;
