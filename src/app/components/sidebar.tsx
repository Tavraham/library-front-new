"use client";
import * as React from "react";
import Box from "@mui/material/Box";
import Drawer from "@mui/material/Drawer";
import AppBar from "@mui/material/AppBar";
import CssBaseline from "@mui/material/CssBaseline";
import Toolbar from "@mui/material/Toolbar";
import List from "@mui/material/List";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import Link from "next/link";
import { Grid } from "@mui/material";
import Logo from "../images/icon _brain_.svg";
import borrow from "../images/icon _notebook and pen_.svg";
import people from "../images/icon _peoples two_.svg";
import book from "../images/icon _book open_.svg";
import { styled } from "@mui/material/styles";

const drawerWidth = 50;
const appBarHeight = 64;

const StyledLink = styled(Link)(({ theme }) => ({
  color: theme.palette.info.main,
}));

export default function Sidebar() {
  return (
    <Box sx={{ display: "flex" }}>
      <CssBaseline />
      <AppBar
        position="fixed"
        sx={{
          backgroundColor: "#D39797",
          height: appBarHeight,
          zIndex: (theme) => theme.zIndex.drawer + 1,
        }}
      >
        <Toolbar sx={{ minHeight: appBarHeight }}>
          <Typography
            variant="h6"
            component="div"
            sx={{ flexGrow: 1 }}
            textAlign={"right"}
            fontFamily="Segoe UI"
          >
            <Grid container>
              <img src={Logo.src} alt={"brain"} loading="lazy" />
              <StyledLink href="/">
                <h1>הספריה</h1>
              </StyledLink>
            </Grid>
          </Typography>
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        anchor="right"
        sx={{
          [`& .MuiDrawer-paper`]: {
            width: "auto",
            backgroundColor: "#D39797",
            marginLeft: drawerWidth,
            top: appBarHeight,
            height: `calc(100% - ${appBarHeight}px)`,
          },
        }}
      >
        <Box>
          <Divider />
          <List>
            {["books", "writers", "borrow"].map((text, index) => (
              <ListItemButton key={text} sx={{ padding: "5px" }}>
                <ListItemIcon
                  sx={{
                    color: "#ffffff",
                    justifyContent: "center",
                    padding: "5px",
                  }}
                >
                  {index === 0 && (
                    <Link href="/book">
                      <img
                        src={book.src}
                        alt={"book"}
                        style={{ filter: "brightness(2)" }}
                      />
                    </Link>
                  )}
                  {index === 1 && (
                    <Link href="/authors">
                      <img src={people.src} alt={"people"} />
                    </Link>
                  )}
                  {index === 2 && (
                    <Link href="/">
                      <img src={borrow.src} alt={"borrow"} />
                    </Link>
                  )}
                </ListItemIcon>
              </ListItemButton>
            ))}
          </List>
        </Box>
      </Drawer>
    </Box>
  );
}
