"use client";

import React from "react";
import { ThemeProvider } from "@emotion/react";
import { Container } from "@mui/material";

export default function Home() {
  return (
    <>
      <div>
        <Container sx={{ marginRight: 10, marginTop: 10 }}>
          <h1>ברוכים הבאים לספריה</h1>
        </Container>
      </div>
    </>
  );
}
