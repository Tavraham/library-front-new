"use client";
import React, { useState, useEffect } from "react";
import { Box, Typography, Grid, Fab } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import questionImage from "../images/icon _help_.svg";
import GenericCard from "../components/GenericCard";
import { useGetAuthorsQuery } from "../store/api";

const AuthorPage = () => {
  const [error, setError] = useState<string | null>(null);

  const {
    data: authors,
    refetch: refetchAuthors,
    error: authorsError,
    isLoading: authorsLoading,
  } = useGetAuthorsQuery();

  useEffect(() => {
    if (authorsError) {
      setError("");
    }
  }, [authorsError]);

  return (
    <>
      <Box sx={{ padding: 10 }}>
        <Typography variant="h5" fontFamily="Segoe UI">
          <header>
            <h2>סופרים</h2>
          </header>
        </Typography>
        {authorsLoading ? (
          <p>Loading...</p>
        ) : authorsError ? (
          <p>Error: {error}</p>
        ) : (
          <Grid container spacing={2}>
            {authors?.map((author) => (
              <Grid
                item
                xs={12}
                sm={4}
                md={4}
                key={author.id}
                container
                justifyContent="center"
              >
                <GenericCard
                  image={questionImage.src}
                  title={author.name}
                  subtitle={""}
                  additionalInfo={""}
                  additionalInfo2={""}
                  bookId={0}
                />
              </Grid>
            ))}
          </Grid>
        )}
        <Fab
          color="primary"
          aria-label="add"
          style={{
            position: "fixed",
            bottom: "20px",
            left: "20px",
            backgroundColor: "#D39797",
          }}
        >
          <AddIcon />
        </Fab>
      </Box>
    </>
  );
};

export default AuthorPage;
