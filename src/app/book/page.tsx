"use client";
import { useEffect } from "react";
import {
  useGetBooksQuery,
  useGetAuthorsQuery,
  usePostBookMutation,
} from "../store/api";
import GenericCard from "../components/GenericCard";
import React, { useState } from "react";
import { Box, Typography, Grid, Fab } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import questionImage from "../images/icon _help_.svg";
import AddForm from "../components/AddForm";

const Books = () => {
  const [open, setOpen] = useState(false);
  const [error, setError] = useState<string | null>(null);

  const {
    data: books,
    refetch: refetchBooks,
    error: booksError,
    isLoading: booksLoading,
  } = useGetBooksQuery();
  const {
    data: authors,
    error: authorsError,
    isLoading: authorsLoading,
  } = useGetAuthorsQuery();
  const [postBook] = usePostBookMutation();

  useEffect(() => {
    if (booksError) {
      setError("טעינת הספרים נכשלה");
    } else if (authorsError) {
      setError("טעינת הסופרים נכשלה");
    }
  }, [booksError, authorsError]);

  const handleAddBook = async (newBook: {
    title: string;
    author: { name: string };
    totalCopies: number;
  }) => {
    try {
      if (!authors) {
        throw new Error("הסופר לא נמצא");
      }

      const author = authors.find(
        (author) => author.name === newBook.author.name
      );

      const bookToPost = {
        title: newBook.title,
        author: author
          ? { id: author.id, name: author.name }
          : { name: newBook.author.name },
        totalCopies: newBook.totalCopies,
      };

      await postBook(bookToPost).unwrap();
      refetchBooks();
    } catch (error) {
      setError("הוספת הספר נכשלה");
    }
  };

  return (
    <>
      <Box sx={{ padding: 10 }}>
        <Typography variant="h5" fontFamily="Segoe UI">
          <header>
            <h2>ספרים</h2>
          </header>
        </Typography>
        {booksLoading || authorsLoading ? (
          <p>Loading...</p>
        ) : booksError || authorsError ? (
          <p>Error: {error}</p>
        ) : (
          <Grid container spacing={2}>
            {books?.map((book) => (
              <Grid
                item
                xs={12}
                sm={4}
                md={4}
                key={book.id}
                container
                justifyContent="center"
              >
                <GenericCard
                  image={questionImage.src}
                  title={book.title}
                  subtitle={book.authorName}
                  additionalInfo={`עותקים זמינים: ${book.availableCopies}`}
                  additionalInfo2={`עותקים: ${book.totalCopies}`}
                  bookId={book.id}
                />
              </Grid>
            ))}
          </Grid>
        )}
        <Fab
          color="primary"
          aria-label="add"
          onClick={() => setOpen(true)}
          style={{
            position: "fixed",
            bottom: "20px",
            left: "20px",
            backgroundColor: "#D39797",
          }}
        >
          <AddIcon />
        </Fab>
        <AddForm
          open={open}
          onClose={() => setOpen(false)}
          onAddBook={handleAddBook}
        />
      </Box>
    </>
  );
};

export default Books;
