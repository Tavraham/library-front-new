import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

const baseUrl = "http://localhost:8080/";

export const api = createApi({
  reducerPath: "api",
  baseQuery: fetchBaseQuery({ baseUrl }),
  endpoints: (builder) => ({
    getBooks: builder.query<Book[], void>({
      query: () => "books",
    }),
    getAuthors: builder.query<Author[], void>({
      query: () => "authors",
    }),
    getBorrowings: builder.query<Borrowing[], number>({
      query: (bookId) => `borrowing/book/${bookId}`,
    }),
    getUsers: builder.query<User[], void>({
      query: () => "users",
    }),
    borrowBook: builder.mutation<void, { userId: number; bookId: number }>({
      query: ({ userId, bookId }) => ({
        url: "borrowing",
        method: "POST",
        body: { userId, bookId },
      }),
    }),
    postBook: builder.mutation<
      void,
      {
        title: string;
        author: { id?: number; name: string };
        totalCopies: number;
      }
    >({
      query: (newBook) => ({
        url: "books",
        method: "POST",
        body: newBook,
      }),
    }),
  }),
});

export const {
  useGetBooksQuery,
  useGetAuthorsQuery,
  useGetBorrowingsQuery,
  useGetUsersQuery,
  useBorrowBookMutation,
  usePostBookMutation,
} = api;

export interface Book {
  id: number;
  title: string;
  authorName: string;
  totalCopies: number;
  availableCopies: number;
}

export interface Author {
  id: number;
  name: string;
}

export interface Borrowing {
  id: number;
  firstName: string;
  lastName: string;
}

export interface User {
  id: number;
  firstName: string;
  lastName: string;
}
