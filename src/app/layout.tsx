"use client";

import React from "react";
import { Inter } from "next/font/google";
import Sidebar from "./components/sidebar";
import ThemeWrapper from "./components/ThemeWrapper";
import { Provider } from "react-redux";
import { store } from "./store/store";
import { Container } from "@mui/material";

const inter = Inter({ subsets: ["latin"] });

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html dir="rtl" lang="en">
      <title>הספריה</title>
      <body className={inter.className}>
        <Provider store={store}>
          <ThemeWrapper>
            <div className="container">
              <Sidebar />
              <div className="page">{children}</div>
            </div>
          </ThemeWrapper>
        </Provider>
      </body>
    </html>
  );
}
